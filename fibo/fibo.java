package myTest.fibo;

/**
 * Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
 * Напоминаем, что первый и второй члены последовательности равны единицам,
 * а каждый следующий — сумме двух предыдущих.
 */
public class fibo
{
    public static void main(String[] args)
    {
        int array [] = new int[20];
        array[0]=1;
        array[1]=1;
        for (int i = 2; i<20; i++)
        {
            array[i]=array[i-1]+array[i-2];
        }
        for (int i=0; i<array.length; i++)
        {
            System.out.println(array[i]);
        }
    }
}
