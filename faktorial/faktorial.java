package myTest.faktorial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Дано число n. При помощи цикла for посчитать факториал n!
 * Примечание. Факториал числа n - произведение всех натуральных чисел от 1 до n включительно.
 * Например, 5! = 1х2х3х4х5 = 120
 */
public class faktorial
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        System.out.println(factorial(n));


    }
// само решение в этой функции
    private static int factorial(int n)
    {
        int f = 1;
        for (int i = 1; i<n+1; i++ )
        {
            f = f*i;
        }
        return f;
    }
}


