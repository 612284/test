package myTest.while_faktorial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Дано число n. При помощи цикла while посчитать факториал n!
 * Примечание. Факториал числа n - произведение всех натуральных чисел от 1 до n включительно.
 * Например, 5! = 1х2х3х4х5 = 120
 */
public class while_faktorial
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        int faktorial = 1;
        int counter = 1;
        while (counter<n+1)
        {
            faktorial = faktorial*counter;
            counter = counter+1;
        }
        System.out.println(faktorial);
    }
}
