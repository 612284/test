package myTest.while1_99;

/**
 *При помощи цикла while вывести на экран нечетные числа от 1 до 99
 */
public class while1_99
{
    public static void main(String[] args)
    {
        int count = 1;
        while (count <100)
        {
            if (count%2>0) System.out.println(count);
            count +=1;
        }
    }
}
