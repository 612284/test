package myTest.for1_99;

/**
 * При помощи цикла for вывести на экран нечетные числа от 1 до 99
 */
public class for1_99
{
    public static void main(String[] args)
    {
       for (int i = 0; i<100; i++)
       {
           if ((i%2)>0) System.out.println(i);
       }
    }
}
