package myTest.two_arreys;

import java.util.Random;

/**
 * Создать двумерный массив из 8 строк по 5 столбцов
 * в каждой из случайных целых чисел из отрезка *10;99+.
 * Вывести массив на экран.
 */
public class two_arreys
{
    public static void main(String[] args)
    {
        int array [][] = new int[8][5];
        Random rnd = new Random();
        for (int i=0; i<8; i++)
        {
            for (int j=0; j<5; j++)
            {
                array[i][j]= rnd.nextInt(89)+10;
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
    }
}
